using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    public float tumble;
    public float speed;
    public GameObject explosion;
    public GameObject playerExplosion;

    void Start()
    {
        GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * tumble;
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }

    private void FixedUpdate()
    {
        float m = transform.position.z - speed;
        Vector3 movement = new Vector3(0.0f, 0.0f, m);
        //GetComponent<Rigidbody>().velocity = movement * speed;
    }

    private void Update()
    {
        if (transform.position.z < -7.0f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player")
        {
            Instantiate(playerExplosion, transform.position, transform.rotation);
        }
        else
        {
            Instantiate(explosion, transform.position, transform.rotation);
        }
        Destroy(other.gameObject);
        Destroy(this.gameObject);
    }
}
