using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour
{
    public GameObject asteroid;
    public Vector3 spawnValue;
    public int asteroidCount;
    public float timeWait;
    public float startWait;
    public float waveWait;

    void Start()
    {
        StartCoroutine(SpawnAsteroid());
    }

    IEnumerator SpawnAsteroid()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < asteroidCount; i++)
            {
                Vector3 spawnPos = new Vector3(Random.Range(-spawnValue.x, spawnValue.x), spawnValue.y, spawnValue.z);
                Quaternion spawnRot = Quaternion.identity;
                Instantiate(asteroid, spawnPos, spawnRot);
                yield return new WaitForSeconds(timeWait);
            }
            yield return new WaitForSeconds(waveWait);
        }
    }
}
