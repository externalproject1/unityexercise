using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Bound
{
    public float minZ, minX, maxZ, maxX;
}

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float tilt;
    public float fireRate;
    public Bound bound;
    public GameObject bullet;
    public Transform spawnPos;
    private Rigidbody rigidbodyObj;
    private float nextFire;

    private void Start()
    {
        rigidbodyObj = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if(Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(bullet, spawnPos.position, spawnPos.rotation);
        }
    }

    private void FixedUpdate()
    {
        float moveH = Input.GetAxis("Horizontal");
        float moveV = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveH, 0.0f, moveV);
        rigidbodyObj.velocity = movement * speed;

        rigidbodyObj.position = new Vector3
        (
            Mathf.Clamp(rigidbodyObj.position.x, bound.minX, bound.maxX),
            0.0f,
            Mathf.Clamp(rigidbodyObj.position.z, bound.minZ, bound.maxZ)
        );
        rigidbodyObj.rotation = Quaternion.Euler(0.0f, 0.0f, rigidbodyObj.velocity.x * -tilt);
    }
}
