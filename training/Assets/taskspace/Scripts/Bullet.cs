using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;

    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }

    private void Update()
    {
        if(transform.position.z > 7.0f)
        {
            Destroy(this.gameObject);
        }
    }
}
