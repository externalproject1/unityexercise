using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBall : MonoBehaviour
{
    public Text txtScore;
    public float speedBall;
    private Rigidbody rigidbodyObj;
    private Text youWin;
    private int score;

    private void Start()
    {
        rigidbodyObj = GetComponent<Rigidbody>();
        youWin = GameObject.Find("winTxt").GetComponent<Text>();
        youWin.gameObject.SetActive(false);
        score = 0;
        txtScore.text = score.ToString();
    }

    private void FixedUpdate()
    {
        if (score < 12)
        {
            float moveH = Input.GetAxis("Horizontal");
            float moveV = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(moveH, 0.0f, moveV);

            rigidbodyObj.AddForce(movement * speedBall);
        }
    }

    private void Update()
    {
        if(score == 12)
        {
            youWin.gameObject.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("pickobj"))
        {
            other.gameObject.SetActive(false);
            score++;
            txtScore.text = score.ToString();
        }
    }
}
