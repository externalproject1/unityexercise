using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject playerBall;

    private Vector3 offset;

    private void Start()
    {
        offset = transform.position - playerBall.transform.position;
    }

    private void LateUpdate()
    {
        transform.position = playerBall.transform.position + offset;
    }
}
